<?php

namespace Flexicast;

use Exception;

class InvalidAttributeCasting extends Exception
{
    public static function castingNotFound($castTo)
    {
        return new static("The $castTo casting was not found.");
    }

    public static function castingNotValid($castTo)
    {
        return new static("The value cannot be cast to a valid $castTo.");
    }
}
