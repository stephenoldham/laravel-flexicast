<?php

namespace Flexicast;

use Flexicast\Traits\ManagesCasting;
use Flexicast\Traits\ManagesQueryScope;

trait HasFlexicastableAttributes
{
    use ManagesCasting, ManagesQueryScope;

    /**
     * Cast an attribute to a native PHP type.
     *
     * @override
     */
    protected function castAttribute($key, $value)
    {
        if($this->isFlexicastable($key)){
            return $this->flexicastForUse($value);
        }

        return parent::castAttribute($key, $value);
    }

    /**
     * Set a given attribute on the model.
     *
     * @override
     */
    public function setAttribute($key, $value)
    {
        if($this->isFlexicastable($key)){
            $this->attributes[$key] = $this->flexicastForStorage($value);

            return $this;
        }

        return parent::setAttribute($key, $value);
    }

    /**
     * Check if an attribute is using flexicasting
     *
     * @return boolean
     **/
    public function isFlexicastable($key)
    {
        $casts = $this->getCasts();
        
        return isset($casts[$key]) && $casts[$key] == 'flexicast';
    }
}