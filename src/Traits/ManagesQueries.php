<?php

namespace Flexicast\Traits;

use Illuminate\Support\Facades\DB;
use Flexicast\InvalidAttributeCasting;

trait ManagesQueries
{
    /**
     * Scope a query by a flexicastable attribute.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereFlexicast($query, $key, $operator, $optional_value = null)
    {
        if(!$this->isFlexicastable($key)) throw InvalidAttributeCasting::attributeNotFlexicastable($key);
        
        // Format the value
        $value = $this->formatFlexicastableQueryValue($key, $operator, $optional_value);

        // Format the key
        $key = $this->formatFlexicastableQueryKey($key);

        return isset($optional_value)? $query->where($key, $operator, $value) : $query->where($key, $value);
    }

    /**
     * Get the correct format for the attribute key
     * based on column type
     *
     * @return string
     **/
    protected function formatFlexicastableQueryKey($key)
    {
        return $this->getFlexicastableAttributeColumnType($key) == 'json'? $key.'->value' : $key;
    }

    /**
     * Get the correct format for the attribute value
     * based on column type and operator
     *
     * @return string
     **/
    public function formatFlexicastableQueryValue($key, $operator, $optional_value = null)
    {
        $value = isset($optional_value)? $optional_value : $operator;

        // First check if we have a LIKE query or a json column
        if((isset($optional_value) && strtoupper($operator) == 'LIKE') || 
            $this->getFlexicastableAttributeColumnType($key) == 'json')
        {
            $cast  = $this->flexicastForStorage($value);
            $value = json_decode($cast, true)['value'];
        }

        return $value;
    }

    /**
     * Get the column type for a castable attribute
     *
     * @return string
     **/
    protected function getFlexicastableAttributeColumnType($key)
    {
        return DB::connection()
                    ->getDoctrineColumn($this->getTable(), $key)
                    ->getType()
                    ->getName();
    }
}