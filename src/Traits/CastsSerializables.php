<?php

namespace Flexicast\Traits;

use Illuminate\Queue\SerializesAndRestoresModelIdentifiers;

trait CastsSerializables
{
	use SerializesAndRestoresModelIdentifiers;
	
	/**
     * Unserialize string value, if it is a valid class
     * Otherwise return the original value.
     *
     * @param string $class
     * @param string $value
     * @return mixed
     */
    protected function toClass($value)
    {
        return unserialize($value);
    }

    /**
     * Serialise a collection for storage.
     * 
     * @param  mixed $value
     * @return mixed
     */
    protected function serializeFlexicastable($value){
        return $this->getSerializedPropertyValue($value);
    }

    /**
     * Restore the model from the model identifier object.
     *
     * @param  object  $value
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function restoreFlexicastableModel($value){
        return $this->restoreModel((object)$value);
    }
    
    /**
     * Restore the collection from the collection identifier object.
     *
     * @param  object  $value
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function restoreFlexicastableCollection($value){
        return $this->restoreCollection((object)$value);
    }
}