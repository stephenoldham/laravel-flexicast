<?php

namespace Flexicast\Traits;

use Exception;
use Carbon\Carbon;
use DateTimeInterface;
use Flexicast\InvalidAttributeCasting;
use Illuminate\Contracts\Queue\QueueableEntity;
use Illuminate\Contracts\Queue\QueueableCollection;
use Illuminate\Support\Collection as BaseCollection;

trait ManagesCasting
{
    use CastsSerializables;

    /**
     * Cast a value
     *
     * @return mixed
     **/
    public function flexicast($value, $castTo = null)
    {
        if(is_null($value)) return null;
        
        if(!is_null($castTo)){
            $this->validateCastTo($value, $castTo);
        }else{
            $castTo = $this->detectCasting($value);
        }

        return $this->castValue($value, $castTo);
    }

    /**
     * Cast the attribute value
     *
     * @return mixed
     **/
    protected function flexicastForUse($value)
    {
        if(is_null($value)) return null;

        $value = $this->fromJson($value);

        return $this->castValue($value['value'], $value['cast_to']);
    }

    /**
     * Format the flexicastable attribute for storage.
     *
     * e.g. [data => 'test', cast_to => 'string']
     *
     * @return array
     **/
    protected function flexicastForStorage($value)
    {
        if(is_null($value)) return null;

        // Check for an explicit casting
        if(is_array($value) 
            && isset($value['value']) 
            && isset($value['cast_to'])) {

            $castTo = $value['cast_to'];
            $value  = $value['value'];

            $this->validateCastTo($value, $castTo);

            // Check is valid
            try {
                $value = $this->castValue($value, $castTo);
            } catch (Exception $e) {
                throw InvalidAttributeCasting::castingNotValid($castTo);
            }
        }else{
            $castTo = $this->detectCasting($value);
        }
            
        return json_encode([
            'value'   => $this->castValueForStorage($value, $castTo),
            'cast_to' => $castTo
        ]);
    }

    /**
     * Validate the attempted casting is allowed
     *
     * @return string
     **/
    protected function validateCastTo($value, $castTo)
    {
        $allowed = [
            'int',
            'integer',
            'real',
            'float',
            'double',
            'string',
            'boolean',
            'object',
            'array',
            'collection',
            'eloquent_collection',
            'eloquent_model',
            'date',
            'datetime',
            'timestamp',
            'class'
        ];

        // Check is allowed
        $isAllowed = in_array($castTo, $allowed);
        if(!$isAllowed) throw InvalidAttributeCasting::castingNotFound($castTo);
    }

    /**
     * Try to auto detect the casting for a value
     *
     * @return string
     **/
    protected function detectCasting($value)
    {
        if(is_null($value)) return null;

        // Numbers
        if(is_numeric($value)){
            if((int)$value == $value) return 'integer';
            if((float)$value == $value) return 'float';
        }

        // Boolean
        if($value === true || $value === false) return 'boolean';

        // Dates
        if($value instanceOf Carbon) return 'datetime';
        if($value instanceOf DateTimeInterface) return 'datetime';

        try {
            if(is_string($value) && $this->isStandardDateFormat($value)) return 'date';
        } catch (Exception $e) {}

        // Collections
        if($value instanceOf QueueableCollection) return 'eloquent_collection';
        if($value instanceOf BaseCollection) return 'collection';

        // Model
        if($value instanceOf QueueableEntity) return 'eloquent_model';

        // Array
        try {
            if((array)$value == $value) return 'array';
        } catch (Exception $e) {}

        // Class
        try {
            if(!$value instanceOf \stdClass && get_class($value)) return 'class';
        } catch (Exception $e) {}

        // Json
        try {
            if(is_array(json_decode($value, true))) return 'json';
        } catch (Exception $e) {}

        // Object
        try {
            if((object)$value == $value) return 'object';
        } catch (Exception $e) {}
        
        // Default
        return 'string';
    }

    /**
     * Casts the value for storage in database.
     *
     * @return string
     **/
    protected function castValueForStorage($value, $castTo)
    {
        // Convert attribute from DateTime to a form proper for storage
        if (in_array($castTo, ['date', 'datetime'])) {
           return $this->fromDateTime($value);
        }

        // Class
        if($castTo == 'class'){
            return serialize($value);
        }

        // Serializables
        if (in_array($castTo, ['eloquent_collection', 'eloquent_model'])) {
            return $this->serializeFlexicastable($value);
        }

        // Taken care of by Laravel core
        // if (in_array($castTo, ['array', 'json', 'object', 'collection'])) {
        //     return json_encode($value);
        // }

        return $value;
    }

    /**
     * Cast meta.
     *
     * @return mixed
     **/
    public function castValue($value, $castTo)
    {
        switch ($castTo) {
            case 'int':
            case 'integer':
                return (int) $value;

            case 'real':
            case 'float':
            case 'double':
                return (float) $value;

            case 'string':
                return (string) $value;

            case 'bool':
            case 'boolean':
                return (bool) $value;

            case 'eloquent_collection':
                if(config('flexicast.suppress.collections')) return $value;

                return $this->restoreFlexicastableCollection($value);

            case 'collection':
                return new BaseCollection($this->fromJson($value));

            case 'eloquent_model':
                if(config('flexicast.suppress.models')) return $value;

                return $this->restoreFlexicastableModel($value);

            case 'date':
                return $this->asDate($value);

            case 'datetime':
                return $this->asDateTime($value);

            case 'timestamp':
                return $this->asTimestamp($value);

            case 'class':
                if(config('flexicast.suppress.classes')) return $value;

                return $this->toClass($value);

            case 'object':
                return (object)$value;

            // We call fromJson to unpack the flexicast attribute so we don't
            // need to call it again for array or json casting
            case 'array':
            case 'json':
            default:
                return $value;
        }
    }
}