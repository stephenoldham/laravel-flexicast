<?php

namespace Flexicast\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

abstract class Model extends EloquentModel
{
    use HasFlexicastableAttributes;
}