<?php

namespace Flexicast\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

abstract class UserModel extends Authenticatable
{
    use HasFlexicastableAttributes;
}