<?php

namespace Flexicast;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;

class FlexicastServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/flexicast.php' => config_path('flexicast.php'),
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/flexicast.php', 'flexicast'
        );
    }
}
