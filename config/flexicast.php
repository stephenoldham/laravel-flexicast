<?php

return [

    'suppress' => [

	    /*
	     * If set to true will disable auto restore for serialized eloquent models
	     */
	    'models' => env('FLEXICAST_SUPPRESS_MODELS', false),

	    /*
	     * If set to true will disable auto restore for serialized eloquent collections
	     */
	    'collections' => env('FLEXICAST_SUPPRESS_COLLECTIONS', false),

	    /*
	     * If set to true will disable auto restore for serialized classes
	     */
	    'classes' => env('FLEXICAST_SUPPRESS_CLASSES', false),

	],

];